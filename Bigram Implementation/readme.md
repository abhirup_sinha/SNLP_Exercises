# ----- PREPROCESSING ----- #
Given a text, your implementation of the preprocess method should perform a preprocessing generating a list of tokens. This preprocessing should include- 1) lowercasing, 2) sentence splitting and 3) the removal of punctuations.

For the input:

She said:"I know that she likes English food!"  
the preprocessing should generate a List<String> containing

[<s>, she, said, i, know, that, she, likes, english, food, </s>]  
(The line above is the output of List.toString(), i.e., the squared brackets and commas are not part of the tokens.)

Your preprocessing should:

mark the beginning of a sentence with <s> and its end with </s>. For simplicity, you can assume that every occurence of ., ! or ? will end a sentence.
remove all non alphanumeric characters, i.e., the tokens should contain only lowercased, alphabetical character or digits. It is known that this will lead to some faulty behavior (e.g., won't will be cut into two words won and t). However, to keep the exercises easy, we will ignore these special cases.
not generate empty tokens or an empty sentence, i.e., <s>, </s> should never occur (while </s>, <s> is of cause necessary).

# ----- BI-GRAM MATRIX ----- #
You should finish the implementation of the given BigramMatrix class. Its constructor takes a list of tokens (the same tokens your preprocess method should create) to generate a count matrix of bi-grams.

Let  𝑐(𝑤𝑖,𝑤𝑗)  be the number of bigrams  (𝑤𝑖,𝑤𝑗) , i.e., the number of times the words  𝑤𝑖  stays in front of the word  𝑤𝑗 . Your bigram matrix should contain a matrix of counts where counts[i][j] contains the value of  𝑐(𝑤𝑖,𝑤𝑗) .

For the input tokens

[<s>, she, said, i, know, that, she, likes, english, food, </s>]  
the matrix looks like the following table (rows are  𝑤𝑖  and columns are  𝑤𝑗 )

	

| `𝑤𝑖\𝑤𝑗` | `<s>` | `</s>` | `english` | `food` | `i` | `know` | `likes` | `said` | `she` | `that` |
|---|---|---|---|---|---|---|---|---|---|---|
| `<s>` | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 1 | 0 |
| `</s>` | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
| `english` | 0 | 0 | 0 | 1 | 0 | 0 | 0 | 0 | 0 | 0 |
| `food` | 0 | 1 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
| `i` | 0 | 0 | 0 | 0 | 0 | 1 | 0 | 0 | 0 | 0 |
| `know` | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 1 |
| `likes` | 0 | 0 | 1 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
| `said` | 0 | 0 | 0 | 0 | 1 | 0 | 0 | 0 | 0 | 0 |
| `she` | 0 | 0 | 0 | 0 | 0 | 0 | 1 | 1 | 0 | 0 |
| `that` | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 1 | 0 | 

* create(List<String> tokens) should initialize the matrix.  
* normalize() should normalize the counts of the matrix (note that a application of the Laplace smoothing should still be possible, even after normalizing).  
* performLaplaceSmoothing() should perform Laplace smoothing on the counts in the matrix (and on the normalized values of the matrix if it has been normalized before)  
* getCount(String word1, String word2) should return the (eventually smoothed) count for the bi-gram (word1, word2).  
* getNormalizedCount(String word1, String word2) should return the (eventually smoothed,) normalized count for the bi-gram (word1, word2) (i.e., the probability  𝑃(𝑤𝑜𝑟𝑑2|𝑤𝑜𝑟𝑑1) ).  

Additionally, you should try to handle unknown words (i.e., words which do not occur in the given text) in a meaningful way, by implementing the following rules:  

* If smoothing has not been applied and one of the words of a bi-gram is not known, return  0 .  
* If the matrix has been smoothed, word1 is know and word2 was not part of the given list of tokens, assume that it is a part of the matrix by returning  1  for its count and  1/𝑠  as its normalization (where  𝑠  is the sum of the row of word1 (without adding the  1  of word2 to this row)).  
* If the matrix has been smoothed and word1 was not part of the given list of tokens, assume that its row was empty before smoothing and contains only  1 s after smoothing.  

You implementation will be tested in 5 different scenarios:  

* The matrix is created and the pure counts are checked.
* The matrix is normalized and the normalized counts are checked.
* The matrix is smoothed and the smoothed counts are checked.
* The matrix is normalized and smoothed and the normalized counts are checked.
* In contrast to the 4 cases above, the matrix is checked with words that do not occur in the text (again in all 4 previous scenarios).