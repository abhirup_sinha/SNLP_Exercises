/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exercise2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Abhirup Sinha
 */
public class sentenceTokenizer {
    /**
     * The token used to indicate the beginning of a new sentence.
     */
    public static final String SENTENCE_START = "<s>";
    /**
     * The token used to indicate the end of a sentence.
     */
    public static final String SENTENCE_END = "</s>";

    /**
     * Preprocesses the given texts and returns the list of tokens.
     *
     * @param text the input text
     * @return the list of tokens extracted from the given text
     */
    public static List<String> preprocess(String text) {
        List<String> tokens = new ArrayList<>();
        // YOUR CODE HERE
        List<String> sentences;
        sentences = Arrays.asList(text.split("[?!.]"));
//        System.out.println(sentences);
        for(String s : sentences){
            s = s.replaceAll("\\W", " "); //replaces all non-alphanumeric character
            s = s.trim().toLowerCase();
            if(s.length() == 0) continue; 
            s = s.replaceAll("^", SENTENCE_START+" ");
            s = s.replaceAll("$", " "+SENTENCE_END);
//            System.out.println(s);
            tokens.addAll(Arrays.asList(s.split("\\s+"))); //splits on whitespaces
        }
        return tokens;
    }
}
