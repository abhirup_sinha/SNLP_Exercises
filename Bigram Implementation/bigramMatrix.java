/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exercise2;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Abhirup Sinha
 */
public class bigramMatrix {
    /**
     * Token used for the start of a sentence.
     */
    public static final String SENTENCE_START = "<s>";
    /**
     * Token used for the end of a sentence.
     */
    public static final String SENTENCE_END = "</s>";
    
    // YOUR CODE HERE
    private double[][] bigramMatrix;
    private double[][] normalizedBigramMatrix;
    private int[] unigramMatrix;
    private String[] vocab;
    private boolean laplaceFlag = false;
    /**
     * Constructor.
     * @param tokens
     */
    public bigramMatrix(List<String> tokens) {
        create(tokens);
    }
    
    private int searchPosition(String[] sequence, String item){
        int found = -1;
        int seq_length = sequence.length;
        for(int i=0;i<seq_length;i++){
            if(sequence[i].equals(item)) found = i; 
        }
        return found;
    }

    /**
     * Internal method for creating the bi-gram matrix from a given list of tokens.
     * 
     * @param tokens
     *            the tokens of an input text for which the matrix should be
     *            initialized.
     */
    
    protected void create(List<String> tokens) {
        // YOUR CODE HERE
        Set<String> word_set = new LinkedHashSet<>();
        word_set.addAll(tokens);
        vocab = word_set.toArray(new String[0]);
        //System.out.println(Arrays.toString(word_set.toArray(new String[0])));
        bigramMatrix = new double[vocab.length][vocab.length];
        unigramMatrix = new int[vocab.length];
        int row_pos, col_pos, pos;
        for(int i=0;i<tokens.size();i++){
            pos = searchPosition(vocab, tokens.get(i));
            unigramMatrix[pos] += 1;
        }
        for(int i=1;i<tokens.size();i++){
            col_pos = searchPosition(vocab, tokens.get(i));
            row_pos = searchPosition(vocab, tokens.get(i-1));
            bigramMatrix[row_pos][col_pos] += 1; 
        }
        /*for(int i=0;i<word_set.size();i++){
            for(int j=0;j<word_set.size();j++)
                System.out.print(bigramMatrix[i][j]+"  ");
            System.out.println();
        }
        for(int i=0;i<word_set.size();i++)
            System.out.print(unigramMatrix[i]+"  ");
        System.out.println();*/
    }

    /**
     * Transforms the internal count matrix into a normalized counts matrix.
     */
    public void normalize() {
        // YOUR CODE HERE
        int pos;
        normalizedBigramMatrix = new double[bigramMatrix.length][];
        for(int i=0;i<bigramMatrix.length;i++)
            normalizedBigramMatrix[i] = bigramMatrix[i].clone();
        for(int row_pos=0;row_pos<vocab.length;row_pos++){
            for(int col_pos=0;col_pos<vocab.length;col_pos++){
                pos = searchPosition(vocab, vocab[col_pos]);
                if(!laplaceFlag) normalizedBigramMatrix[col_pos][row_pos] /= unigramMatrix[pos];
                if(laplaceFlag) normalizedBigramMatrix[col_pos][row_pos] /= (unigramMatrix[pos]+vocab.length);
            }
        }
        /*for(int i=0;i<vocab.length;i++){
            for(int j=0;j<vocab.length;j++){
                System.out.printf("%.2f", normalizedBigramMatrix[i][j]);
                System.out.print("  ");}
            System.out.println();
        }*/
    }

    /**
     * Performs the Laplace smoothing on the bi-gram matrix.
     */
    public void performLaplaceSmoothing() {
        // YOUR CODE HERE
        for(int row_pos=0;row_pos<vocab.length;row_pos++){
            for(int col_pos=0;col_pos<vocab.length;col_pos++)
                bigramMatrix[row_pos][col_pos] += 1;
        }
        laplaceFlag = true;
        if(normalizedBigramMatrix != null) normalize();
    }

    /**
     * Returns the count of the bi-gram matrix for the bi-gram (word1, word2).
     */
    public double getCount(String word1, String word2) {
        double count = 0;
        // YOUR CODE HERE
        int row_pos, col_pos;
        col_pos = searchPosition(vocab, word2);
        row_pos = searchPosition(vocab, word1);
        try{
            count = bigramMatrix[row_pos][col_pos];
        }
        catch(ArrayIndexOutOfBoundsException a){
            //a.printStackTrace();
            if(laplaceFlag) count = 1;
            if(!laplaceFlag) count = 0;
        }
        return count;
    }

    /**
     * Returns the normalized count of the bi-gram matrix for the bi-gram (word1, word2) (i.e., P(word2 | word1)).
     */
    public double getNormalizedCount(String word1, String word2) {
        double normalizedCount = 0;
        // YOUR CODE HERE
        int row_pos, col_pos;
        col_pos = searchPosition(vocab, word2);
        row_pos = searchPosition(vocab, word1);
        try{
            normalizedCount = normalizedBigramMatrix[row_pos][col_pos];
        }
        catch(ArrayIndexOutOfBoundsException a){
            //a.printStackTrace();
            if(!laplaceFlag) normalizedCount = 0;
            else if(laplaceFlag){
                if((searchPosition(vocab, word2)<0) && (searchPosition(vocab, word1)>=0)){
                    //System.out.println(Arrays.toString(normalizedBigramMatrix[searchPosition(vocab, word1)]));
                    //System.out.println(Arrays.stream(normalizedBigramMatrix[searchPosition(vocab, word1)]).sum());
                    int pos = searchPosition(vocab, word1);
                    normalizedCount = (1 / Arrays.stream(normalizedBigramMatrix[pos]).sum()) /(unigramMatrix[pos]+vocab.length);
                }
                else if((searchPosition(vocab, word2)>=0) && (searchPosition(vocab, word1)<0)){
                    //After smoothing, C(W_i,W_j) = 1, C(W_i) = 1. So, C(W_i,W_j)+1/C(W_i)+|V| -> 1/|V|
                    normalizedCount =  (double) 1 / vocab.length;
                }
                else if((searchPosition(vocab, word2)<0) && (searchPosition(vocab, word1)<0)){
                    //Both non-present. After smoothing, C(W_i) = 1, C(W_j) = 1, C(W_i,W_j) = 1. So, (C(W_i,W_j)+1)/(C(W_i)+|V|) -> 1/|V|
                    normalizedCount =  (double) 1 / vocab.length;
                }
            }
        }
        return normalizedCount;
    }
}
