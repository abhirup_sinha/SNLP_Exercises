/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bigram;

/**
 *
 * @author Abhirup Sinha
 */
public class Main {
    public static void main(String[] args){
        String text1 = "She said:\"I know that she likes English food!\"";
        String text2 = "London is the capital and largest city of England. Million people live in London. The river Thames is in London. London is the largest city in Western-Europe.";
        
        System.out.println("----- 1st example -----");
        bigramMatrix bm = new bigramMatrix(sentenceTokenizer.preprocess(text1));
        System.out.println("Checking Raw Bigram counts:- ");
        System.out.println("she said: "+bm.getCount("she", "said"));
        System.out.println("english food: "+bm.getCount("english", "food"));
        System.out.println("likes food: "+bm.getCount("likes", "food"));
        
        System.out.println("Checking Normalized Bigram counts:- ");
        bm.normalize();
        System.out.println("she said: "+bm.getNormalizedCount("she", "said"));
        System.out.println("english food: "+bm.getNormalizedCount("english", "food"));
        System.out.println("likes food: "+bm.getNormalizedCount("likes", "food"));
        
        System.out.println("Checking Smoothed Bigram counts:- ");
        bm.performLaplaceSmoothing();
        System.out.println("she said: "+bm.getCount("she", "said"));
        System.out.println("english food: "+bm.getCount("english", "food"));
        System.out.println("likes food: "+bm.getCount("likes", "food"));
        
        System.out.println("Checking Normalized Smoothed Bigram counts:- ");
        bm.normalize();
        System.out.println("she said: "+bm.getNormalizedCount("she", "said"));
        System.out.println("english food: "+bm.getNormalizedCount("english", "food"));
        System.out.println("likes food: "+bm.getNormalizedCount("likes", "food"));       
        
        System.out.println("----- 2nd example -----");
        bm = new bigramMatrix(sentenceTokenizer.preprocess(text2));
        System.out.println("Checking Raw Bigram counts:- ");
        System.out.println("london </s>: "+bm.getCount("london", "</s>"));
        System.out.println("largest city: "+bm.getCount("largest", "city"));
        System.out.println("river thames: "+bm.getCount("river", "thames"));
        System.out.println("city river: "+bm.getCount("city", "river"));
        
        System.out.println("Checking Normalized Bigram counts:- ");
        bm.normalize();
        System.out.println("london </s>: "+bm.getNormalizedCount("london", "</s>"));
        System.out.println("largest city: "+bm.getNormalizedCount("largest", "city"));
        System.out.println("river thames: "+bm.getNormalizedCount("river", "thames"));
        System.out.println("city river: "+bm.getNormalizedCount("city", "river"));
        
        System.out.println("Checking Smoothed Bigram counts:- ");
        bm.performLaplaceSmoothing();
        System.out.println("london </s>: "+bm.getCount("london", "</s>"));
        System.out.println("largest city: "+bm.getCount("largest", "city"));
        System.out.println("river thames: "+bm.getCount("river", "thames"));
        System.out.println("city river: "+bm.getCount("city", "river"));
        
        System.out.println("Checking Normalized Smoothed Bigram counts:- ");
        bm.normalize();
        System.out.println("london </s>: "+bm.getNormalizedCount("london", "</s>"));
        System.out.println("largest city: "+bm.getNormalizedCount("largest", "city"));
        System.out.println("river thames: "+bm.getNormalizedCount("river", "thames"));
        System.out.println("city river: "+bm.getNormalizedCount("city", "river"));
        
        System.out.println("----- Test with unknown words -----");
        bm = new bigramMatrix(sentenceTokenizer.preprocess(text2));  //set the matrix back
        System.out.println("Checking Raw Bigram counts:- ");
        System.out.println("london underground: "+bm.getCount("london", "underground"));
        System.out.println("small city: "+bm.getCount("small", "city"));
        System.out.println("sky scraper: "+bm.getCount("sky", "scraper"));
        
        System.out.println("Checking Normalized Bigram counts:- ");
        bm.normalize();
        System.out.println("london underground: "+bm.getNormalizedCount("london", "underground"));
        System.out.println("small city: "+bm.getNormalizedCount("small", "city"));
        System.out.println("sky scraper: "+bm.getNormalizedCount("sky", "scraper"));
        
        System.out.println("Checking Smoothed Bigram counts:- ");
        bm.performLaplaceSmoothing();
        System.out.println("london underground: "+bm.getCount("london", "underground"));
        System.out.println("small city: "+bm.getCount("small", "city"));
        System.out.println("sky scraper: "+bm.getCount("sky", "scraper"));
        
        System.out.println("Checking Normalized Smoothed Bigram counts:- ");
        bm.normalize();
        System.out.println("london underground: "+bm.getNormalizedCount("london", "underground"));
        System.out.println("small city: "+bm.getNormalizedCount("small", "city"));
        System.out.println("sky scraper: "+bm.getNormalizedCount("sky", "scraper"));
        System.out.println("europe eastern: "+bm.getNormalizedCount("europe", "eastern"));
    } 
}
