/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exercise2;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Abhirup Sinha
 */
public class Main {
    public static void main(String[] args) throws IOException{
    
        System.out.println("----- Testing on short example ----");
        String text = "London is the capital and largest city of England. Million people " + 
        "live in London. The River Thames is in London. London is the largest city in " +
        "Western Europe.";

        TrigramBasedSpellCorrector corrector = new TrigramBasedSpellCorrector(text);
        
        System.out.println(corrector.getCorrection("largest", "city", "im")); //should return 'in'
        System.out.println(corrector.getCorrection("largest", "city", "of")); //should return 'in' or 'of'
        System.out.println(corrector.getCorrection("London", "is", "teh")); //should return 'the'
        System.out.println(corrector.getCorrection("largest", "capital", "in")); //should return null
        System.out.println(corrector.getCorrection("natural", "language", "processing")); //should return null
        
        System.out.println("----- Testing on Shakespeare example ----");
        // Read text of Shakespeare
        File file = new File("./shakespeare.txt");
        String text9 = FileUtils.readFileToString(file, "UTF-8");
        long time = System.currentTimeMillis();
        TrigramBasedSpellCorrector corrector1 = new TrigramBasedSpellCorrector(text9);
        time = System.currentTimeMillis() - time;
        System.out.println("Loading the tri-grams from Shakespeare took " + time + "ms.");
        if(time > 60000) {
            System.out.println("Loading the tri-grams took very long. You may want to check your implementation.");
        }
        System.out.println(corrector1.getCorrection("The", "river", "stüx")); //should return 'styx'
        System.out.println(corrector1.getCorrection("The", "River", "Stüx")); //should return 'styx'
        System.out.println(corrector1.getCorrection("ambassadors", "from", "noway")); //should return 'norway'
        System.out.println(corrector1.getCorrection("first", "noble", "Frodo")); //should return 'friend'
        System.out.println(corrector1.getCorrection("the", "devil", "siaeks")); //should return "speaks" OR "rides"
    } 
}
