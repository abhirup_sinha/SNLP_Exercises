/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exercise2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

/**
 *
 * @author Abhirup Sinha
 */
public class TrigramBasedSpellCorrector {
    
    private final String SENTENCE_START = "<s>";
    private final String SENTENCE_END = "</s>";
    private Hashtable<String, Integer> trigramFreqTable;
    private Hashtable<String, ArrayList<String>> trigramWordTable;
    private Hashtable<String, Integer> bigramTable;
    private String[] vocab;
    
    /**
     * Constructor.
     */
    public TrigramBasedSpellCorrector(String text) {
        create(text);
    }
    
    /**
     * Internal methods for tokenization of text
     */
    private List<String> preprocess(String text) {
        List<String> tokens = new ArrayList<>();
        List<String> sentences;
        sentences = Arrays.asList(text.split("[?!.]"));
        for(String s : sentences){
            s = s.replaceAll("\\W", " "); //replaces all non-alphanumeric character
            s = s.trim().toLowerCase();
            if(s.length() == 0) continue; 
            s = s.replaceAll("^", SENTENCE_START+" ");
            s = s.replaceAll("$", " "+SENTENCE_END);
            tokens.addAll(Arrays.asList(s.split("\\s+"))); //splits on whitespaces
        }
        return tokens;
    }
    
    /**
     * Internal methods for calculating Levenshtein Distance between two words
     */
    private int calculateDistance(String string1, String string2) {
    int distance;
    int len_s1 = string1.length();
    int len_s2 = string2.length();
    int[][] distanceMatrix = new int[len_s1+1][len_s2+1];
    for(int i=1;i<=len_s1;i++) distanceMatrix[i][0] = i;
    for(int j=1;j<=len_s2;j++) distanceMatrix[0][j] = j;
    for(int i=1;i<=len_s1;i++){
        for(int j=1;j<=len_s2;j++){
            if(string1.charAt(i-1) == string2.charAt(j-1)) distanceMatrix[i][j] = distanceMatrix[i-1][j-1];
            else distanceMatrix[i][j] = Math.min(distanceMatrix[i-1][j-1], Math.min(distanceMatrix[i-1][j], distanceMatrix[i][j-1])) + 1;
        }
    }
    distance = distanceMatrix[len_s1][len_s2];
    return distance;
    }
    
    /**
     * Internal methods for determining the necessary statistics about the tri-grams
     * of the given text.
     */
    protected void create(String text) {
        // YOUR CODE HERE
        List<String> tokens = preprocess(text);
        //Set<String> word_set = new LinkedHashSet<>();
        //word_set.addAll(tokens);
        //vocab = word_set.toArray(new String[0]);
        trigramFreqTable = new Hashtable<>();
        trigramWordTable = new Hashtable<>();
        //bigramTable = new Hashtable<>();
        String trigramString, tempBigramString, trigramToken;
        //String bigramString;
        for(int i=0;i<tokens.size()-2;i++){
            tempBigramString = tokens.get(i)+" "+tokens.get(i+1);
            trigramToken = tokens.get(i+2);
            trigramString = tempBigramString+" "+trigramToken;
            if(!trigramFreqTable.containsKey(trigramString)) trigramFreqTable.put(trigramString, 1);
            else if(trigramFreqTable.containsKey(trigramString)) trigramFreqTable.put(trigramString, trigramFreqTable.get(trigramString)+1);
            if(!trigramWordTable.containsKey(tempBigramString)){
                trigramWordTable.put(tempBigramString, new ArrayList<>());
                trigramWordTable.get(tempBigramString).add(trigramToken);
            }
            else if(trigramWordTable.containsKey(tempBigramString)){
                trigramWordTable.get(tempBigramString).add(trigramToken);
                //System.out.println("Bigram String: "+tempBigramString+", Trigram Tokens: "+trigramWordTable.get(tempBigramString));
            }
        }
        for(int i=0;i<tokens.size()-1;i++){
            String bigramString = tokens.get(i)+" "+tokens.get(i+1);
            if(!bigramTable.containsKey(bigramString)) bigramTable.put(bigramString, 1);
            else if(bigramTable.containsKey(bigramString)) bigramTable.put(bigramString, bigramTable.get(bigramString)+1);
        }
    }
    
    /**
     * Returns the correction of the third word based on the internal tri-grams that
     * start with word1 and word2 as well as the Levenshtein distance of candidates
     * from these tri-grams to the given word3.
     * 
     * @return a word for which a tri-gram with word1 and word2 at the beginning
     *         exists and which has the smallest Levenshtein distance to the given
     *         word3. Or null, if such a word does not exist.
     */
    public String getCorrection(String word1, String word2, String word3) {
        // YOUR CODE HERE
        String correctWord = "";
        word1 = word1.replaceAll("\\W", "").toLowerCase();
        word2 = word2.replaceAll("\\W", "").toLowerCase();
        word3 = word3.replaceAll("\\W", "").toLowerCase();
        int Distance = Integer.MAX_VALUE;
        String bigramString;
        List<String> possibleTrigramTokens;
        bigramString = word1+" "+word2;
        //System.out.println(bigramString);
        possibleTrigramTokens = trigramWordTable.get(bigramString);
        //System.out.println(possibleTrigramTokens);
        try{
            for(String word:possibleTrigramTokens){
                if(calculateDistance(word,word3) < Distance){
                    correctWord = word;
                    Distance = calculateDistance(word,word3);
                }
            }
        }
        catch(NullPointerException npe){
            correctWord = null; //no trigram starts with word1 and word2
        }
        return correctWord;
    }
}
