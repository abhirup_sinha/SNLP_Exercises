/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NaiveBayesClassification;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Abhirup Sinha
 */
// YOUR CODE HERE

/**
 * Classifier implementing naive Bayes classification.
 */
public class BayesianClassifier {
    // YOUR CODE HERE
    private final Set<String> docVocab;
    private final Map<String, Integer>  classFrequency;
    private final Map<String, Map<String, Integer>> classVocab;
    private final Set<String> stopwords;
    private int total_class_occurence;
    private final int min_word_size;
    
    BayesianClassifier(Set<String> docVocab, Map<String, Integer> classFrequency, Map<String, Map<String, Integer>> classVocab, Set<String> stopwords, int min_word_size) {
        total_class_occurence = 0;
        this.docVocab = docVocab;
        this.classFrequency = classFrequency;
        this.classVocab = classVocab;
        this.stopwords = stopwords;
        this.min_word_size = min_word_size;
        total_class_occurence = classFrequency.keySet().stream().map(key -> classFrequency.get(key)).reduce(total_class_occurence, Integer::sum);
    }
    
    public List<String> preprocess(String text) {
        List<String> tokens = new ArrayList<>();
        // YOUR CODE HERE        
        text = text.replaceAll("[^a-zA-Z]", " "); //replaces all non-alphanumeric character
        text = text.trim().toLowerCase();
//      System.out.println(text);
        tokens.addAll(Arrays.asList(text.split("\\s+"))); //splits on whitespaces
        return tokens;
    }
    
    /**
     * Classifies the given document and returns the class name.
     * @param text
     * @return class
     */
    public String classify(String text) {
        String clazz = null;
        // YOUR CODE HERE
        System.out.println(text);
        double classProba = Double.NEGATIVE_INFINITY;
        List<String> text_tokens = preprocess(text);
        for(String clas : classFrequency.keySet()) {
            double classPrior = (double) classFrequency.get(clas) / total_class_occurence;
            double classWordSize = 0;
            double likelihood = 0L;
            Map<String, Integer> vocab = classVocab.get(clas);
            for(String s : vocab.keySet())
                classWordSize += vocab.get(s);
            for(String token : text_tokens) {
                if(!stopwords.contains(token) & token.length() >= min_word_size) {
//                    System.out.println("Likelihood: "+likelihood);
//                    System.out.println("Total Words in Class: "+classWordSize);
//                    System.out.println("Current Word: "+token+"\t Word Frequency: "+vocab.get(token));
                    int wordFreq;
                    try {
                        wordFreq = vocab.get(token);
                    } catch(NullPointerException e) {
                        wordFreq = 0;
                    }
                    likelihood += Math.log((wordFreq + 1) / (classWordSize + docVocab.size()));
//                    likelihood *= (wordFreq + 1) / (classWordSize + docVocab.size());
                }
            }
//            double temp_proba = likelihood + Math.log(classPrior);
            double temp_proba = likelihood + Math.log(0.5);
//            double temp_proba = (double) likelihood * classPrior;
//            System.out.println("Class:"+clas+"\tClass Prior:"+classPrior+"\tLikelihood:"+likelihood+"\tProbability:"+temp_proba);
            if(temp_proba > classProba) {
                classProba = temp_proba;
                clazz = clas;
            }
            System.out.println("Current Class -> "+clazz);
        }
//        System.out.println(clazz+"\t"+text);
        return clazz;
    }
}

