/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NaiveBayesClassification;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.io.FileUtils;

public class Main {
    /**
    * Simple method for reading classification examples from a file as a list of (class, text) pairs.
     * @param filename
     * @throws java.io.IOException
    */
    public static List<String[]> readClassData(String filename) throws IOException {
    return FileUtils.readLines(new File(filename), "utf-8").stream().map(s -> s.split("\t"))
            .filter(s -> s.length > 1).collect(Collectors.toList());
    }
    
    /**
    * Method for cecking the given classifier. The method expects training and evaluation data.
    * The data should have a String array for each document in which the first cell of the
    * array contains the class while the second cell contains the text of the document. During
    * the check, some statistics like the accuracies of different baseline classifiers are
    * printed. Finally, the calculated accuracy is returned.
    *
    * @param trainingCorpus the data that is used for training the classifier. 
    * @param evaluationCorpus the data that is used for evaluating the classifier. 
    * @param minAccuracy minimum accuracy the classifier should achieve.
    * @return the accuracy achieved by the classifier
    */
    public static double checkClassifier(List<String[]> trainingCorpus, List<String[]> evaluationCorpus,
            double minAccuracy) {
            double accuracy = 0;
            System.out.print("Training corpus size: ");
            System.out.println(trainingCorpus.size());
            System.out.print("Eval. corpus size   : ");
            System.out.println(evaluationCorpus.size());
            // Determine the classes
            Set<String> classes = Arrays.asList(trainingCorpus, evaluationCorpus).stream().flatMap(l -> l.stream())
                    .map(d -> d[0]).distinct().collect(Collectors.toSet());
            // Determine the number of instances per class in the evaluation set
            Map<String, Long> evalClassCounts = evaluationCorpus.stream()
                    .collect(Collectors.groupingBy(d -> d[0], Collectors.counting()));
            for (String clazz : classes) {
                if (!evalClassCounts.containsKey(clazz)) {
                    evalClassCounts.put(clazz, 0L);
                }
            }

            // Determine the expected accuracies of the baselines
            Map<String, Double> accForClassGuessers = new HashMap<>();
            for (Entry<String, Long> e : evalClassCounts.entrySet()) {
                accForClassGuessers.put(e.getKey(), e.getValue() / (double) evaluationCorpus.size());
            }
            double accRandomGuesser = 1.0 / accForClassGuessers.size();

            // Train the classifier
            long time1 = System.currentTimeMillis();
            BayesianLearner learner = new BayesianLearner(classes);
            for (String[] trainingExample : trainingCorpus) {
                learner.learnExample(trainingExample[0], trainingExample[1]);
            }
            BayesianClassifier classifier = learner.createClassifier();
            time1 = System.currentTimeMillis() - time1;
            System.out.println("Training took       : " + time1 + "ms");

            // Classify the evaluation corpus
            long time2 = System.currentTimeMillis();
            int tp = 0, errors = 0, id = 0;
            String result;
            List<String[]> fpDetails = new ArrayList<>();
            for (String[] evalExample : evaluationCorpus) {
                result = classifier.classify(evalExample[1]);
                if (evalExample[0].equals(result)) {
                    ++tp;
                } else {
                    ++errors;
                    fpDetails.add(new String[] { Integer.toString(id), evalExample[0], result });
                }
                ++id;
            }
            time2 = System.currentTimeMillis() - time2;
            System.out.println("Classification took : " + time2 + "ms");
            accuracy = tp / (double) (tp + errors);

            System.out.println("Baseline classifiers: ");
            for (Entry<String, Double> baseResult : accForClassGuessers.entrySet()) {
                System.out.println(String.format("Always %-13s: %-7.5f", baseResult.getKey(), baseResult.getValue()));
            }
            System.out.println(String.format("Random guesser      : %-7.5f", accRandomGuesser));
            System.out.println(String.format("Your solution       : %-7.5f (%d tp, %d errors)", accuracy, tp, errors));
            if (fpDetails.size() > 0) {
                System.out.println("  Wrong classifications are:");
                for (int i = 0; i < Math.min(fpDetails.size(), 20); ++i) {
                    System.out.print("    id=");
                    System.out.print(fpDetails.get(i)[0]);
                    System.out.print(" expected=");
                    System.out.print(fpDetails.get(i)[1]);
                    System.out.print(" result=");
                    System.out.println(fpDetails.get(i)[2]);
                }
                if (fpDetails.size() > 20) {
                    System.out.println("    ...");
                }
            }

            // Make sure that the students solution is better than all baselines
            for (Entry<String, Double> baseResult : accForClassGuessers.entrySet()) {
                if (baseResult.getValue() >= accuracy) {
                    StringBuilder builder = new StringBuilder();
                    builder.append("Your solution is not better than a classifier that always chooses the \"");
                    builder.append(baseResult.getKey());
                    builder.append("\" class.");
                    System.out.println(builder.toString());
                }
            }
            if (accRandomGuesser >= accuracy) {
                System.out.println("Your solution is not better than a random guesser.");
            }
            if ((minAccuracy > 0) && (minAccuracy > accuracy)) {
                System.out.println("Your solution did not reach the expected accuracy of " + minAccuracy);
            }
            System.out.println("Test successfully completed.");
            return accuracy;
        }
    
    public static void main(String[] args) throws IOException {
    /*
    * Test case 1: a simple example corpus which is easy to do by hand.
    */
    System.out.println("---------- Simple example corpus ----------");
    List<String[]> exampleCorpusTrain = Arrays.asList(
        new String[] {"chess", "white king, black rook, black queen, white pawn, black knight, white bishop." },
        new String[] {"history", "knight person granted honorary title knighthood" },
        new String[] {"history", "knight order eligibility, knighthood, head of state, king, prelate, middle ages." },
        new String[] {"chess", "Defense knight pawn opening game opponent." },
        new String[] {"literature", "Knights Round Table. King Arthur. literary cycle Matter of Britain."}
        );
    List<String[]> exampleCorpusTest = Arrays.asList(
        new String[] {"history", "Knighthood Middle Ages." },
        new String[] {"chess", "player king knight opponent king checkmate game draw." },
        // document with unknown words
        new String[] {"literature", "britain king arthur. Sir Galahad." }
        );

    double accuracy = checkClassifier(exampleCorpusTrain, exampleCorpusTest, 0);


    /*
     * Test case 2: a more complex example on real-world data.
     */
    System.out.println();
    System.out.println("---------- Larger example corpus ----------");
    List<String[]> classificationData =readClassData("./single-class-train.tsv");
    accuracy = checkClassifier(classificationData.subList(0, 750), classificationData.subList(750, classificationData.size()), 0);
    }
}