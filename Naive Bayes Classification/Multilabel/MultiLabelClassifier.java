/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MultiLabelClassification;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Abhirup Sinha
 */
/**
 * Classifier implementing naive Bayes classification for a multilabel
 * classification.
 */
public class MultiLabelClassifier {
    // YOUR CODE HERE
    private final Set<String> stopwords;
    private final Map<String, Integer>  classFrequency;
    private final Map<String, Map<String, Integer>> classVocab;
    private final List<String> classes;
    private final Set<String> docVocab;
    private int total_class_occurence;
    private final double epsilon = 1E-2;
    private final int min_word_size;
    
    MultiLabelClassifier(Set<String> docVocab, Map<String, Integer> classFrequency, Map<String, Map<String, Integer>> classVocab, Set<String> stopwords, int min_word_size) {
        this.docVocab = docVocab;
        this.classFrequency = classFrequency;
        this.classVocab = classVocab;
        this.stopwords = stopwords;
        classes = new ArrayList<>(classFrequency.keySet());
        this.min_word_size = min_word_size;
        total_class_occurence = classFrequency.keySet().stream().map(key -> classFrequency.get(key)).reduce(total_class_occurence, Integer::sum);
    }
    
    public List<String> preprocess(String text) {
        List<String> tokens = new ArrayList<>();
        // YOUR CODE HERE        
        text = text.replaceAll("[^a-zA-Z0-9]", " "); //replaces all non-alphanumeric character
        text = text.trim().toLowerCase();
//      System.out.println(text);
        tokens.addAll(Arrays.asList(text.split("\\s+"))); //splits on whitespaces
        return tokens;
    }
    
    /**
     * Classifies the given document and returns the class names.
     * @param text
     * @return result
     */
    public Set<String> classify(String text) {
        Set<String> results = new HashSet<>();
        // YOUR CODE HERE
//        System.out.println(text);
        List<Double> temp_proba = new ArrayList<>();
        Map<Double,String> classProbMap = new LinkedHashMap();
        List<String> text_tokens = preprocess(text);
        for(String clas : classes) {
            double classPrior = (double) classFrequency.get(clas) / total_class_occurence;
            double classWordSize = 0;
            double likelihood = 0L;
            Map<String, Integer> vocab = classVocab.get(clas);
            for(String s : vocab.keySet())
                classWordSize += vocab.get(s);
            for(String token : text_tokens) {
                if(!stopwords.contains(token) && token.length() >= min_word_size) {
                    int wordFreq;
                    try {
                        wordFreq = vocab.get(token);
                    } catch(NullPointerException e) {
                        wordFreq = 0;
                    }
                    likelihood += Math.log((wordFreq + 1) / (classWordSize + docVocab.size()));
                }
            }
            Double prob = likelihood + Math.log(classPrior);
            temp_proba.add(prob);
            classProbMap.put(prob, clas);
//            System.out.println("Class: "+clas+"\tClass Prior: "+classPrior+"\tLikelihood: "+likelihood);
            
//            System.out.println("Temporary Probability -> "+temp_proba.get(temp_proba.size()-1));
//            System.out.println("Current Class Probability -> "+classProba);
        }
        Collections.sort(temp_proba);
//        System.out.println(temp_proba);
        double max_prob = temp_proba.remove(temp_proba.size()-1);
 //       System.out.println(temp_proba +" -> "+ max_prob);
        results.add(classProbMap.get(max_prob));
        for(double prob:temp_proba){
            if(Math.abs(max_prob - prob) < Math.abs(Math.log(epsilon))) results.add(classProbMap.get(prob));
        }
        return results;
    }
}
