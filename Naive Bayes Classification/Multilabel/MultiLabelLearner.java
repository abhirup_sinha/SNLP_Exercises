/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MultiLabelClassification;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Abhirup Sinha
 */
/**
 * Learner (or Builder) class for a naive Bayes multilabel classifier.
 */
public class MultiLabelLearner {
    // YOUR CODE HERE
    protected Set<String> docVocab;
    protected Map<String, Integer>  classFrequency;
    protected Map<String, Map<String, Integer>> classVocab;
    private final int min_word_size = 3;
    private final Set<String> stopwords = new HashSet<>(Arrays.asList("i","me","my","myself","we","our","ours","ourselves","you","your","yours",
    "yourself","yourselves","he","him","his","himself","she","her","hers","herself","it","its","itself","they","them",
    "their","theirs","themselves","what","which","who","whom","this","that","these","those","am","is","are","was",
    "were","be","been","being","have","has","had","having","do","does","did","doing","a","an","the","and","but","if",
    "or","because","as","until","while","of","at","by","for","with","about","against","between","into","through",
    "during","before","after","above","below","to","from","up","down","in","out","on","off","over","under","again",
    "further","then","once","here","there","when","where","why","how","all","any","both","each","few","more","most",
    "other","some","such","no","nor","not","only","own","same","so","than","too","very","s","t","can","will","just",
    "don","should","now","reuter","today","tomorrow","yesterday","day","month","year","previous","next","after",
    "before","january","february","march","april","may","june","july","august","september","october","november","december"));
    /**
     * Constructor taking the number of classes the classifier should be able to
     * distinguish.
     * @param classes
     */
    public MultiLabelLearner(Set<String> classes) {
        // YOUR CODE HERE
        classFrequency = new HashMap();
        classVocab = new HashMap();
        docVocab = new HashSet();
        Iterator it = classes.iterator();
        while (it.hasNext()) {
            String item = it.next().toString();
            classFrequency.put(item, 0);
            classVocab.put(item, new HashMap());
        }
    }
    
    /*private int getIndex(Set<? extends Object> set, Object value) {
    int result = 0;
    for (Object entry:set) {
        if (entry.equals(value)) return result;
        result++;
    }
    return -1;
    }*/
    
    private List<String> preprocess(String text) {
        List<String> tokens = new ArrayList<>();
        // YOUR CODE HERE        
        text = text.replaceAll("[^a-zA-Z0-9]", " "); //replaces all non-alphanumeric character
        text = text.trim().toLowerCase();
//      System.out.println(text);
        tokens.addAll(Arrays.asList(text.split("\\s+"))); //splits on whitespaces
        return tokens;
    }

    /**
     * The method used to learn the training examples.It takes the names of the
 classes as well as the text of the training document.
     * @param classes
     * @param text
     */
    public void learnExample(Set<String> classes, String text) {
        // YOUR CODE HERE
        for(String cls : classes) {
            classFrequency.put(cls, classFrequency.get(cls)+1);
            List<String> text_tokens = preprocess(text);
            Map<String, Integer> freqMap = classVocab.get(cls);
            for(String item : text_tokens){
                try{
                    if(!stopwords.contains(item) && item.length() >= min_word_size) freqMap.put(item, freqMap.get(item)+1);
                } catch (NullPointerException e) {
                    if(!stopwords.contains(item) && item.length() >= min_word_size) freqMap.put(item, 1);
                }
            }
            classVocab.put(cls, freqMap);
        }
    }

    /**
     * Creates a MultiLabelClassifier instance based on the statistics gathered from
     * the training example.
     * @return 
     */
    public MultiLabelClassifier createClassifier() {
        MultiLabelClassifier classifier = null;
        // YOUR CODE HERE
        docVocab = new HashSet<>();
        Set<String> classes = classFrequency.keySet();
        for(String clas : classes)
            docVocab.addAll(classVocab.get(clas).keySet());
        classifier = new MultiLabelClassifier(docVocab, classFrequency, classVocab, stopwords, min_word_size);
        return classifier;
    }
}
