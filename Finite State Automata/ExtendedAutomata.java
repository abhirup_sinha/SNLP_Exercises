package FSA;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Abhirup Sinha
 */

/*

In the second Task of this exercise, the extension ExtendedAutomata should be enabled to extract all 
matching Strings from a given text. The class is already inheriting the functionalities of your Automata class.

Hints
* The automata does not have to support very complex grammars (e.g., not all features of regular expressions).
* Make sure that you have executed the cell with your implementation of the Automata class to 
  make sure that the Kernel is aware of your class implementation.
* Make sure that if two matches are overlapping, the longest match is extracted. 
  This is not an issue with the given test grammar. 
  However, for the grammar of the regular expression ab*, an automaton should 
  extract from the given string "a abbb b" the complete second term abbb instead of ab or abb.

*/
public class ExtendedAutomata extends Automata{ 
    
    public ExtendedAutomata(String grammarDef) {
        super(grammarDef);
    }
    
    /**
     * Searches for all matchings of the grammar from the given text.
     * @param text
     * @return matches
     */
    
    public String[] findMatches(String text){
        List<String> matches = new ArrayList<>();
        String partial_match_string;
        int reject_state = -1;
        int start_state = 0;
        List<Integer> accept_state = new ArrayList<>();
        for(int i=1;i<trans_table.length;i++){
            if(reject_state < Integer.parseInt(String.valueOf(trans_table[i][0].charAt(0))))
                reject_state = i-1;
            if(start_state >= Integer.parseInt(String.valueOf(trans_table[i][0].charAt(0))))
                start_state = i-1;
            if(trans_table[i][0].contains(":")) accept_state.add(i-1);
        }
        String[] text_arr = text.split("\\s");
        int string_length;
        for(String t:text_arr){
            if(acceptsString(t)) matches.add(t);
            if(!acceptsString(t)){
                List<String> part_matches = new ArrayList<>();
                partial_match_string = "";
                string_length = t.length();
                int current_state = start_state;
                int next_state;
                for(int i=0;i<string_length;i++){
                    try{
//                        System.out.println("Current Character: "+String.valueOf(t.charAt(i))+" Current State: "+current_state);
                        next_state = Integer.parseInt(trans_table[current_state+1][searchPosition(trans_table[0], String.valueOf(t.charAt(i)))]);
//                        System.out.println("Next State: "+next_state);
                        if(next_state == reject_state){
                            if(current_state == start_state){
//                                System.out.println("PART_MATCH R=S "+partial_match_string);
                                continue;
                            }
                            if(current_state != start_state){
//                                System.out.println("PART_MATCH R!=S "+partial_match_string);
                                partial_match_string = "";
                                next_state = current_state;
                            }
                        }
                        partial_match_string = partial_match_string.concat(String.valueOf(t.charAt(i)));
                        if(accept_state.contains(next_state)){
//                            System.out.println("PART_MATCH ACCEPTED "+partial_match_string);
                            part_matches.add(partial_match_string);
                            next_state = start_state;
                        }
                        current_state = next_state;
                    }
                    catch(ArrayIndexOutOfBoundsException a){
//                        System.out.println("PART_MATCH NOT IN TABLE "+partial_match_string);
                        current_state = start_state;
                        partial_match_string = "";
                    }
                }
                if(!part_matches.isEmpty()){
                    int pos = 0;
                    int large_length = part_matches.get(pos).length();
                    for(int i=1;i<part_matches.size();i++){
                        if(large_length < part_matches.get(i).length()){
                            large_length = part_matches.get(i).length();
                            pos = i;
                        }
                    }
                matches.add(part_matches.get(pos));
                }
            }
        }
        return matches.toArray(new String[0]);       
    }
}
