package FSA;

/**
 *
 * @author Abhirup Sinha
 */

/*
You should finish the class Automata. It should be able to-
1. read a given grammar and
2. check whether a given text is accepted by the grammar or not.
In the second Task of this exercise, the extension ExtendedAutomata should be 
enabled to extract all matching Strings from a given text (described later in more detail).

*/
public class Main {

    /**
     * @param args the command line arguments
     */    
    public static void main(String args[]) {
        // TODO code application logic here
        String grammar = "State\ta\tb\t!\n0\t5\t1\t5\n1\t2\t5\t5\n2\t3\t5\t5\n3\t3\t5\t4\n4:\t5\t5\t5\n5\t5\t5\t5";
        Automata automata = new Automata(grammar);
        System.out.println(automata.acceptsString("baa!")); //TRUE for grammar
        System.out.println(automata.acceptsString("baaa!")); //TRUE for grammar
        System.out.println(automata.acceptsString("baaa!!!")); //FALSE for grammar
        System.out.println(automata.acceptsString("!aab")); //FALSE for grammar
        System.out.println(automata.acceptsString("xyz")); //FALSE for grammar
        
//        ExtendedAutomata eautomata1 = new ExtendedAutomata(grammar);
//        System.out.println(Arrays.toString(eautomata1.findMatches("baa! He said baaaa! babaa!! baaaa!")));
//        System.out.println(Arrays.toString(eautomata1.findMatches("baa! He said baca!! xxaabaaaaa!!! baaaa! babaa!! baaaa!")));
    }
}
