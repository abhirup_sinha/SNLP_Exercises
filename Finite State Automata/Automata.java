package FSA;

/**
 *
 * @author Abhirup Sinha
 */

/*

We are using the baa! sheep example from the slides but your implementation 
has to accept other grammars as well. The transition matrix is given as a tab separated String:

State    a    b    !
0    5    1    5
1    2    5    5
2    3    5    5
3    3    5    4
4:    5    5    5
5    5    5    5

(Note that this notebook renders the example above with whitespaces but the data in the tests will use tabs.) 
For the line endings, the \n character is used (might be important for Windows users!).

The transition matrix contains a head line and after that one state per line.
The head line starts with the String State. After that, the single input characters are listed.
The single status lines show the transitions from the left state to the other states 
given the input characters defined in the head state. 
For example, there is a transition from state 0 to 5 if a or ! are read and a transition to 1 if b is read.
The end state(s) are marked with :. In the example, state 4 is the only end state.

Hints
* 0 is always the start state.
* There can be multiple end states.
* The state with the highest ID in the grammar is always the error state (in the example, it is the state 5). 
  This is the state the automata gets into if a character is read that does not fit 
  to the grammar at that particular position. It can also be seen that the automata can not leave the error state.
* All characters that are not listed in the head should directly lead to the error state.
* The lines below the head line don't have to be ordered (i.e., the states can be defined in any order).
* The class Automata has two methods - parseGrammar and acceptsString. 
  The first method is called when constructing the Automata and should parse the grammar. 
  Later, the second method is called and should calculate a result based on the grammar and the input String. 
  Note that you may want to add some class attributes to make sure that you can store the parsed information 
  (i.e., the transitions) in the parseGrammar method to be able to use them later on in the acceptsString method.

*/
public class Automata {

    protected String[][] trans_table;
    private int row_length;
    private int col_length;
    
    public Automata(String grammarDef) {
        parseGrammar(grammarDef);
    }
    
    protected void parseGrammar(String grammarDef) {        
        // YOUR CODE HERE
        String[] states = grammarDef.split("\\n");
        row_length = states.length;
        col_length = states[0].split("\\t").length;
        trans_table = new String[row_length][col_length];
        for(int s=0;s<row_length;s++){
            trans_table[s] = states[s].split("\\t");
        }
    }
    protected int searchPosition(String[] sequence, String item){
        int found = -1;
        int seq_length = sequence.length;
        for(int i=0;i<seq_length;i++){
            if(sequence[i].equals(item)) found = i; 
        }
        return found;
    }
    
    public boolean acceptsString(String text) {
        boolean accepted = false;
        int current_state = 0;
        int current_state_index = 0;
        for(int j=1;j<row_length;j++){
            if(trans_table[j][0].equals(String.valueOf(current_state)))
                current_state_index = j;
        }
        int text_length = text.length();
        for(int i=0;i<text_length;i++){
            try{                           
//                System.out.println("Current Character: "+String.valueOf(text.charAt(i))+" Current State: "+current_state+" at index: "+current_state_index);
                String next_state = trans_table[current_state_index][searchPosition(trans_table[0], String.valueOf(text.charAt(i)))];
//                System.out.println("Next State: "+next_state);
                current_state = Integer.parseInt(next_state.strip());
                for(int j=1;j<row_length;j++){
                    if(trans_table[j][0].equals(String.valueOf(current_state)))
                        current_state_index = j;
                    else if(trans_table[j][0].equals(String.valueOf(current_state)+":"))
                        current_state_index = j;
                }
            }
            catch(ArrayIndexOutOfBoundsException a){
//                System.out.println("Unknown Character Encountered");
                break;
            }
        }
        if(trans_table[current_state_index][0].contains(":")) accepted = true;
        return accepted;
    }
}