/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HMM;

import java.util.Arrays;
import java.util.List;

/**
 * A class implementing the Viterbi algorithm based on an Hidden Markov Model
 * with the given states, observations, transition matrix and emission matrix.
 * 
 * You may want to copy it from Exercise-1.
 */
/**
 *
 * @author Abhirup Sinha
 */
public class ViterbiAlgorithm {
    // YOUR CODE HERE
    private final String[] states;
    private final List<String> observationVocab;
    private final double[][] transitionMatrix;
    private final double[][] emissionMatrix;
    /**
     * Constructor.
     * @param states
     * @param observationVocab
     * @param transitionMatrix
     * @param emissionMatrix
     */
    public ViterbiAlgorithm(String[] states, String[] observationVocab, double[][] transitionMatrix,
            double[][] emissionMatrix) {
        // YOUR CODE HERE
        this.states = states;
        this.emissionMatrix = emissionMatrix;
        this.observationVocab = Arrays.asList(observationVocab);
        this.transitionMatrix = transitionMatrix;
    }

    /**
     * Returns the sequence of states which has the highest probability to create
     * the given sequence of observations.
     * 
     * @param observations
     *            a sequence of observations
     * @return the sequence of states
     */
    public StateSequence getStateSequence(String[] observations) {
        StateSequence sequence = null;
        // YOUR CODE HERE
        //        System.out.println(Arrays.toString(observations));
        double[][] viterbi = new double[states.length+2][observations.length+1];
        for(double[] vit : viterbi)
            Arrays.fill(vit, Double.NEGATIVE_INFINITY);
        int[][] backpointer = new int[states.length+2][observations.length+1];
        for(int s=1; s<=states.length; s++) {
//            System.out.println("State - "+states[s-1]);
//            System.out.println("Viterbi[s][1] - "+viterbi[s][1]);
//            System.out.println("transitionMatrix[0]["+s+"] - "+transitionMatrix[0][s]);
//            System.out.println("First Observation - "+observations[0]);
//            System.out.println("First Observation Vocab Index - "+observationVocab.indexOf(observations[0]));
//            System.out.println("Emission Likelihood - "+emissionMatrix[s-1][observationVocab.indexOf(observations[0])]);
            viterbi[s][1] = Math.log(transitionMatrix[0][s]) + Math.log(emissionMatrix[s-1][observationVocab.indexOf(observations[0])]);
//            System.out.println("Viterbi["+s+"][1] - "+viterbi[s][1]);
            backpointer[s][1] = 0;
//            System.out.println("backpointer["+s+"][1] - "+backpointer[s][1]);
        }
        for(int T=1; T<observations.length; T++) {
//            System.out.println("Observation - "+observations[T]);
            for(int s=1; s<=states.length; s++) {
                for(int s1=1; s1<=states.length; s1++) {
//                    System.out.println("viterbi["+s+"]["+T+"] - "+viterbi[s][T]);
//                    System.out.println("transitionMatrix["+s+"]["+s1+"] - "+transitionMatrix[s][s1]);
//                    System.out.println("Emission Probability - "+emissionMatrix[s1-1][observationVocab.indexOf(observations[T])]);
                    double tempProba = viterbi[s1][T] + Math.log(transitionMatrix[s1][s]) + 
                            Math.log(emissionMatrix[s-1][observationVocab.indexOf(observations[T])]);
                    if(tempProba > viterbi[s][T+1]) {
                        viterbi[s][T+1] = tempProba;
                        backpointer[s][T+1] = s1;
//                        System.out.println("viterbi["+s1+"]["+(T+1)+"] - "+viterbi[s1][T+1]);
//                        System.out.println("backpointer["+s1+"]["+(T+1)+"] - "+backpointer[s1][T+1]);
                    }
//                    else System.out.println(tempProba+" is not greater than "+viterbi[s1][T+1]);
                }
            }
        }
        for(int s=1;s<=states.length;s++) {
            double endProba = viterbi[s][observations.length] + Math.log(transitionMatrix[s][states.length+1]);
            if (endProba > viterbi[states.length+1][observations.length]) {
                viterbi[states.length+1][observations.length] = endProba;
                backpointer[states.length+1][observations.length] = s;
            }
        }
        /*System.out.println("--- VITERBI MATRIX ---");
        for(int i=0;i<=states.length+1;i++) {
            for(int j=0;j<=observations.length;j++)
                System.out.print(String.format("%.4f",viterbi[i][j])+"  ");
            System.out.println();
        }
        
        System.out.println("--- BACKPOINTER MATRIX ---");
        for(int i=0;i<=states.length+1;i++) {
            for(int j=0;j<=observations.length;j++)
                System.out.print(backpointer[i][j]+"  ");
            System.out.println();
        }*/
        String[] stateSequence = new String[observations.length];
//        System.out.println("Ultimate Back Pointer - "+backpointer[states.length+1][observations.length]);
        stateSequence[observations.length-1] = states[backpointer[states.length+1][observations.length]-1];
        int bp = backpointer[states.length+1][observations.length];
        for(int i=observations.length; i>1 ;i--) {
            stateSequence[i-2] = states[backpointer[bp][i]-1];
            bp = backpointer[bp][i];
        }
//        System.out.println(Arrays.toString(stateSequence));
        sequence = new StateSequence(stateSequence, viterbi[states.length+1][observations.length]);
        return sequence;
    }
}
