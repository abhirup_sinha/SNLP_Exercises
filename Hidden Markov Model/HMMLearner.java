/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HMM;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Abhirup Sinha
 */
public class HMMLearner {
    // YOUR CODE HERE
    private String[] states;
    private String[] observationVocab;
    private double[][] transitionMatrix;
    private double[][] emissionMatrix;
    private Map<String, Integer> totalStateSet;
    private Map<String, Integer> conditionalStateSet;
    private Map<String, Integer> transStartEnd;
    private Map<String, Integer> transitionMap;
    private Set<String> observationVocabSet;
    private int totalLinesRead;
    private final String SENTENCE_START = "<s>";
    private final String SENTENCE_END = "</s>";
    
    public HMMLearner(int numberOfStates, int sizeOfVocab) {
        // YOUR CODE HERE
        states = new String[numberOfStates];
        observationVocab = new String[sizeOfVocab];
        transitionMatrix = new double[numberOfStates+2][numberOfStates+2];
        emissionMatrix = new double[numberOfStates][sizeOfVocab];
        conditionalStateSet = new HashMap<>();
        totalStateSet = new HashMap<>();
        transStartEnd = new HashMap<>();
        transitionMap = new HashMap<>();
        observationVocabSet = new HashSet<>();
        totalLinesRead = 0;
    }
    
    private void processObservation(String observation) {
        if(observation.contains(SENTENCE_START)) {
            observation = observation.replace(SENTENCE_START, "");
            String start_state = observation.split("_")[1];
            if(start_state.contains(SENTENCE_END)) //FOR SINGLE OBSERVATIONS
                start_state = start_state.replace(SENTENCE_END, "");
            transStartEnd.put(SENTENCE_START+" "+start_state, transStartEnd.getOrDefault(SENTENCE_START+" "+start_state, 0)+1);
        }
        if(observation.contains(SENTENCE_END)) {
            observation = observation.replace(SENTENCE_END, "");
            String end_state = observation.split("_")[1];
            transStartEnd.put(end_state+" "+SENTENCE_END, transStartEnd.getOrDefault(end_state+" "+SENTENCE_END, 0)+1);
        }
        String[] obs = observation.split("_");
        observationVocabSet.add(obs[0]);
        totalStateSet.put(obs[1], totalStateSet.getOrDefault(obs[1], 0)+1);
        conditionalStateSet.put(observation, conditionalStateSet.getOrDefault(observation, 0)+1);
    }
    
    private void processStates(String sequence) {
        sequence = sequence.replaceAll(SENTENCE_START,"");
        sequence = sequence.replaceAll(SENTENCE_END,"");
        sequence = sequence.replaceAll("\\w*_", "");
//        System.out.println(sequence);
        String[] state = sequence.split(" ");
        if(state.length > 1) {
            for(int i=1;i<state.length;i++)
                transitionMap.put(state[i-1]+" "+state[i], transitionMap.getOrDefault(state[i-1]+" "+state[i], 0)+1);
        }
    }

    public void processSequence(String sequence) {
        // YOUR CODE HERE
        totalLinesRead += 1;
        sequence = sequence.replaceAll("^",SENTENCE_START);
        sequence = sequence.replaceAll("$",SENTENCE_END);
        String[] annotated_obs = sequence.split(" ");
        for (String obs : annotated_obs)
            processObservation(obs);
        processStates(sequence);
    }
    
    public void myPrint() {
        System.out.println("Total Observations -> "+totalLinesRead);
        System.out.println("Vocabulary -> "+Arrays.toString(observationVocabSet.toArray()));
        for(String state : totalStateSet.keySet())
            System.out.println(state +" -> "+ totalStateSet.get(state));
        for(String cond_state : conditionalStateSet.keySet())
            System.out.println(cond_state +" -> "+ conditionalStateSet.get(cond_state));
        for(String ex_state : transStartEnd.keySet())
            System.out.println(ex_state +" -> "+ transStartEnd.get(ex_state));
        for(String state : transitionMap.keySet())
            System.out.println(state +" -> "+ transitionMap.get(state));
    }

    public ViterbiAlgorithm buildViterbi() {
        ViterbiAlgorithm viterbi = null;
        // YOUR CODE HERE
        observationVocabSet.toArray(observationVocab);
        totalStateSet.keySet().toArray(states);
//        System.out.println(Arrays.toString(observationVocab));
//        System.out.println(Arrays.toString(states));
        for(int i=0;i<states.length;i++) {
            for(int j=0;j<observationVocab.length;j++)
                emissionMatrix[i][j] = (double) (conditionalStateSet.getOrDefault(observationVocab[j]+"_"+states[i], 0) + 1) / (totalStateSet.getOrDefault(states[i], 0) + observationVocab.length);
        }
        /*for(int i=0;i<states.length;i++) {
            for(int j=0;j<observationVocab.length;j++)
                System.out.printf("%.2f\t",emissionMatrix[i][j]);
            System.out.println();
        }*/
        for(int i=0;i<states.length;i++) {
            int denominator = totalStateSet.getOrDefault(states[i], 0) + observationVocab.length;
            transitionMatrix[0][i+1] = (double) (transStartEnd.getOrDefault(SENTENCE_START+" "+states[i], 0) + 1) / (totalLinesRead + observationVocab.length);          
            transitionMatrix[i+1][states.length+1] = (double) (transStartEnd.getOrDefault(states[i]+" "+SENTENCE_END, 0) + 1) / denominator;
            for(int j=0;j<states.length;j++) 
                transitionMatrix[i+1][j+1] = (double) (transitionMap.getOrDefault(states[i]+" "+states[j], 0) + 1) / denominator;                        
        }       
        /*for(int i=0;i<states.length+2;i++) {
            for(int j=0;j<states.length+2;j++)
                System.out.printf("%.2f\t",transitionMatrix[i][j]);
            System.out.println();
        }*/
        viterbi = new ViterbiAlgorithm(states, observationVocab, transitionMatrix, emissionMatrix);
        return viterbi;
    }
}
