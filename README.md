# README #

This repository contains my implementation of some of the commonly used algorithms in the realm of "Natural Language Processing".

**Course Name:** Statistical Natural Language Processing  
**Duration:** Winter Semester 2020/21  
**Location:** Paderborn University, Germany  

### How do I get set up? ###

* You can clone this repository to get the source files, but make sure to amend package name. 
* These files can be opened using any standard Java IDE. 
* I have used JDK 11 for the exercise, but I think the code will work for JDK 8 onwards. If not, install JDK 11. 
* Some implementations may need "Apache Commons IO" jar file

### Who do I talk to? ###

* Repo owner (i.e. myself)
* Drop me a mail on [mailme@abhirupsinha.com](mailto:mailme@abhirupsinha.com)
